
import React from 'react'

import './styles.css'

import SkillBar from '../skillBar/SkillBar'

const skills = [
    {
        id: 1,
        name: 'HTML',
        progress: '70%',
        color: '#2B43F6'
    },
    {
        id: 2,
        name: 'CSS',
        progress: '60%',
        color: '#2B43F6'
    },
    {
        id: 3,
        name: 'Javascript',
        progress: '65%',
        color: '#2B43F6'
    },
    {
        id: 4,
        name: 'Nodejs',
        progress: '40%',
        color: '#2B43F6'
    },
    {
        id: 5,
        name: 'Expressjs',
        progress: '40%',
        color: '#2B43F6'
    },
    {
        id: 6,
        name: 'Reactjs',
        progress: '50%',
        color: '#2B43F6'
    },
    {
        id: 7,
        name: 'Mongodb',
        progress: '50%',
        color: '#2B43F6'
    },
    {
        id: 8,
        name: 'Mysql',
        progress: '30%',
        color: '#2B43F6'
    }
]

function SkillSoftware() {
    return (
        <div className="skillSoftwareContainer">
            {
                skills.map(skill => (
                    <div className="skills" key={skill.id}>
                        <SkillBar name={skill.name} progress={skill.progress} color={skill.color}/>   
                    </div>
                ))
            }
        </div>
    )
}

export default SkillSoftware
