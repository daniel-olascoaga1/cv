
import React from 'react';

import Presentation from './Presentation'
import SkillsMain from './SkillsMain'
import CardsPortafolio from  './CardsPortafolio'
import Footer from  './Footer'

function Portafolio() {
    return (
        <div>
            <Presentation />
            <SkillsMain />
            <CardsPortafolio />
            <Footer />
        </div>
    )
}

export default Portafolio
