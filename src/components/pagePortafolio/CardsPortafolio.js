import React from 'react'

import CardsSoftware from './CardsSoftware'
import CardsElectronics from  './CardsElectronics'

function CardsPortafolio() {
    return (
        <div className="cardsPortafolio">
            <h2>Portafolio</h2>
            <h3>Proyectos de Software</h3>
            <CardsSoftware/>
            <h3>Proyectos de electronica</h3>
            <CardsElectronics />
            <button><a href="https://github.com/">More Projects</a></button>
        </div>
    )
}

export default CardsPortafolio
