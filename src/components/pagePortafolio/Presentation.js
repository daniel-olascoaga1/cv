
import React from 'react'

import Photo from '../assets/foto2.jpg'

function Presentation() {
    return (
        <div className="presentation">
            <header>
                <div className="personalPhoto">
                    <img src={Photo} alt=""/>
                </div>
                <div className="personalDescription">
                    <h2>Daniel Olascoaga</h2>
                    <h3>Frontend developer and electronic technology</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque iusto eum ratione fuga eius tenetur iure esse! Corporis, voluptatibus illo.</p>
                    <button>More Me</button>
                </div>
            </header>
        </div>
    )
}

export default Presentation
