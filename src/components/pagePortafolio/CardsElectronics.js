
import React from 'react'

import './styles.css'
import ProjectElectronic from '../assets/projectElectronic.jpg'

import Card from '../card/Card'

const cards = [
    {
        id: 1,
        title: 'PROJECT1',
        image: ProjectElectronic,
        link: 'https://github.com'
    },
    {
        id: 2,
        title: 'PROJECT2',
        image: ProjectElectronic,
        link: 'https://github.com'
    },
    {
        id: 3,
        title: 'PROJECT3',
        image: ProjectElectronic,
        link: 'https://github.com'
    },
    {
        id: 4,
        title: 'PROJECT4',
        image: ProjectElectronic,
        link: 'https://github.com'
    }
]

function CardsElectronics() {
    return (
        <div className="container">
            {
                cards.map(card => (
                    <div className="cards" key={card.id}>
                        <Card title={card.title} imageSource={card.image} link={card.link}/>
                    </div>
                ))
            }
        </div>
    )
}

export default CardsElectronics
