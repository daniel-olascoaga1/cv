
import React from 'react'

import './styles.css'

function Footer() {
    return (
        <div className="footer">
            <footer>
                <h2>&copy; Portafolio Daniel Olascoaga</h2>
                <h3>2021</h3>
                <h3>All rights Reserved</h3>
            </footer>
        </div>
    )
}

export default Footer
