
import React, { Component } from 'react'

import SkillBar from '../skillBar/SkillBar'

const skills = [
    {
        id: 1,
        name: "Mantenimiento",
        progress: '70%',
        color: 'darkgreen'
    },
    {
        id: 2,
        name: "Telecomunicaciones",
        progress: '30%',
        color: 'darkgreen'
    },
    {
        id: 3,
        name: "Electronica de potencia",
        progress: '70%',
        color: 'darkgreen'
    },
    {
        id: 4,
        name: "Instrumentación y control",
        progress: '60%',
        color: 'darkgreen'
    },
    {
        id: 5,
        name: "Microcontroladores",
        progress: '65%',
        color: 'darkgreen'
    },
    {
        id: 6,
        name: "IoT",
        progress: '50%',
        color: 'darkgreen'
    },
    {
        id: 7,
        name: "CAD en electronica (eagle, kicad)",
        progress: '80%',
        color: 'darkgreen'
    }      
      

]

export class SkillElectronic extends Component {
    render() {
        return (
            <div className="skillElectronicContainer">
                {
                    skills.map(skill => (
                        <div className="skills" key={skill.id}>
                            <SkillBar name={skill.name} progress={skill.progress} color={skill.color} />
                        </div>
                    ))
                }
            </div>
        )
    }
}

export default SkillElectronic
