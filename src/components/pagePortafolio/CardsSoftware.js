
import React from 'react'

import './styles.css'
import ProjectSoftware from '../assets/projectSoftware.jpg'

import Card from '../card/Card'

const cards = [
    {
        id: 1,
        title: 'PROJECT1',
        image: ProjectSoftware,
        link: 'https://github.com'
    },
    {
        id: 2,
        title: 'PROJECT2',
        image: ProjectSoftware,
        link: 'https://github.com'
    },
    {
        id: 3,
        title: 'PROJECT3',
        image: ProjectSoftware,
        link: 'https://github.com'
    },
    {
        id: 4,
        title: 'PROJECT4',
        image: ProjectSoftware,
        link: 'https://github.com'
    }
]

function CardsSoftware() {
    return (
        <div className="container">
            {
                cards.map(card => (
                    <div className="cards" key={card.id}>
                        <Card title={card.title} imageSource={card.image} link={card.link}/>
                    </div>
                ))
            }
        </div>
    )
}

export default CardsSoftware
