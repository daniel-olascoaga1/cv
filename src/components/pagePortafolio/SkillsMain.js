
import React from 'react'

import SkillSoftware from './SkillSoftware'
import SkillElectronic from './SkillElectronic'

function SkillsMain() {
    return (
        <div className="skillsContainer"> 
            <main>
                <section>
                    <div className="skillBars">
                        <h2>SKILLS SOFTWARE</h2>
                        <SkillSoftware />   
                        <h2>SKILLS ELECTRONIC</h2>
                        <SkillElectronic />
                    </div>
                    <div className="experience">
                        <h2>EXPERIENCE</h2>
                    </div>
                </section>
            </main>
        </div>
    )
}

export default SkillsMain
