
import './navigation.css'

import React from 'react'
import {Link} from 'react-router-dom'

function Navigation() {
    return (
        <div className="topnav">
            <nav>
                <ul>
                    <li className="nav-link">
                        <Link className="link" to="/">Portafolio</Link>
                    </li>
                    <div className="nav-links-container">
                        <li className="nav-links-contained">
                            <Link className="link" to="/blog">Blog</Link>
                        </li>
                        <li className="nav-links-contained">
                            <Link className="link" to="/github">Github</Link>
                        </li>
                    </div>
                </ul>
            </nav>
        </div>
    )
}

export default Navigation
