

import './card.css';
import React from 'react';
import PropTypes from 'prop-types'

function Card(props) {
    return (
        <div className="card">
            <img src={props.imageSource} alt=""/>
            <div className="container">
                <h4><b>{props.title}</b></h4>
                <p> 
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, natus.
                </p>
                <button><a href={props.link}>More Info</a></button>
            </div>
        </div>
    )
}

Card.propTypes = {
    title: PropTypes.string.isRequired,
    imageSource: PropTypes.string,
    link: PropTypes.string
}

export default Card
