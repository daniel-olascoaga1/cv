
import React from 'react'

import './skillBar.css'

function SkillBar(props) {
    return (
        <div className="skillBar">
            <h3>{props.name}</h3>
            <div className="container">
                <div className="progress" style={{width: (props.progress),backgroundColor: (props.color)}}>
                    {props.progress}
                </div>
            </div>
        </div>
    )
}

export default SkillBar
