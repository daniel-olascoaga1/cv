
import './App.css';

import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import Navigation from './components/navigation/Navigation'
import Portafolio from './components/pagePortafolio/Portafolio'
import Blog from './components/pageBlog/Blog'
import Github from './components/pageGithub/Github'

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />

        <Switch>
          <Route exact path="/">
            <Portafolio />
          </Route>
          <Route exact path="/blog">
            <Blog />
          </Route>
          <Route exact path="/github">
            <Github />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
